#!/bin/bash

openapi-generator generate -i commerce_taxonomy_v1_oas3.yaml -g php -o . --additional-properties=apiPackage=Api,invokerPackage=MyBit\\Ebay\\Taxonomy,artifactVersion=1.1.0,composerPackageName=my-bit/ebay-taxonomy-api