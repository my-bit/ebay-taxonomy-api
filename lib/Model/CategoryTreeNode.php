<?php
/**
 * CategoryTreeNode
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Taxonomy
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Taxonomy API
 *
 * Use the Taxonomy API to discover the most appropriate eBay categories under which sellers can offer inventory items for sale, and the most likely categories under which buyers can browse or search for items to purchase. In addition, the Taxonomy API provides metadata about the required and recommended category aspects to include in listings, and also has two operations to retrieve parts compatibility information.
 *
 * The version of the OpenAPI document: v1.0.1
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace MyBit\Ebay\Taxonomy\Model;

use \ArrayAccess;
use \MyBit\Ebay\Taxonomy\ObjectSerializer;

/**
 * CategoryTreeNode Class Doc Comment
 *
 * @category Class
 * @description This type contains information about all nodes of a category tree or subtree hierarchy, including and below the specified &lt;b&gt;Category&lt;/b&gt;, down to the leaf nodes. It is a recursive structure.
 * @package  MyBit\Ebay\Taxonomy
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class CategoryTreeNode implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'CategoryTreeNode';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'category' => '\MyBit\Ebay\Taxonomy\Model\Category',
        'category_tree_node_level' => 'int',
        'child_category_tree_nodes' => '\MyBit\Ebay\Taxonomy\Model\CategoryTreeNode[]',
        'leaf_category_tree_node' => 'bool',
        'parent_category_tree_node_href' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'category' => null,
        'category_tree_node_level' => 'int32',
        'child_category_tree_nodes' => null,
        'leaf_category_tree_node' => null,
        'parent_category_tree_node_href' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'category' => false,
        'category_tree_node_level' => false,
        'child_category_tree_nodes' => false,
        'leaf_category_tree_node' => false,
        'parent_category_tree_node_href' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'category' => 'category',
        'category_tree_node_level' => 'categoryTreeNodeLevel',
        'child_category_tree_nodes' => 'childCategoryTreeNodes',
        'leaf_category_tree_node' => 'leafCategoryTreeNode',
        'parent_category_tree_node_href' => 'parentCategoryTreeNodeHref'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'category' => 'setCategory',
        'category_tree_node_level' => 'setCategoryTreeNodeLevel',
        'child_category_tree_nodes' => 'setChildCategoryTreeNodes',
        'leaf_category_tree_node' => 'setLeafCategoryTreeNode',
        'parent_category_tree_node_href' => 'setParentCategoryTreeNodeHref'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'category' => 'getCategory',
        'category_tree_node_level' => 'getCategoryTreeNodeLevel',
        'child_category_tree_nodes' => 'getChildCategoryTreeNodes',
        'leaf_category_tree_node' => 'getLeafCategoryTreeNode',
        'parent_category_tree_node_href' => 'getParentCategoryTreeNodeHref'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('category', $data ?? [], null);
        $this->setIfExists('category_tree_node_level', $data ?? [], null);
        $this->setIfExists('child_category_tree_nodes', $data ?? [], null);
        $this->setIfExists('leaf_category_tree_node', $data ?? [], null);
        $this->setIfExists('parent_category_tree_node_href', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets category
     *
     * @return \MyBit\Ebay\Taxonomy\Model\Category|null
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param \MyBit\Ebay\Taxonomy\Model\Category|null $category category
     *
     * @return self
     */
    public function setCategory($category)
    {
        if (is_null($category)) {
            throw new \InvalidArgumentException('non-nullable category cannot be null');
        }
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets category_tree_node_level
     *
     * @return int|null
     */
    public function getCategoryTreeNodeLevel()
    {
        return $this->container['category_tree_node_level'];
    }

    /**
     * Sets category_tree_node_level
     *
     * @param int|null $category_tree_node_level The absolute level of the current category tree node in the hierarchy of its category tree.<br><br><span class=\"tablenote\"> <strong>Note:</strong> The root node of any full category tree is always at level <code><b>0</b></code>. </span>
     *
     * @return self
     */
    public function setCategoryTreeNodeLevel($category_tree_node_level)
    {
        if (is_null($category_tree_node_level)) {
            throw new \InvalidArgumentException('non-nullable category_tree_node_level cannot be null');
        }
        $this->container['category_tree_node_level'] = $category_tree_node_level;

        return $this;
    }

    /**
     * Gets child_category_tree_nodes
     *
     * @return \MyBit\Ebay\Taxonomy\Model\CategoryTreeNode[]|null
     */
    public function getChildCategoryTreeNodes()
    {
        return $this->container['child_category_tree_nodes'];
    }

    /**
     * Sets child_category_tree_nodes
     *
     * @param \MyBit\Ebay\Taxonomy\Model\CategoryTreeNode[]|null $child_category_tree_nodes An array of one or more category tree nodes that are the immediate children of the current category tree node, as well as their children, recursively down to the leaf nodes.<br><br><i>Returned only if</i> the current category tree node is not a leaf node (the value of <b>leafCategoryTreeNode</b> is <code>false</code>).
     *
     * @return self
     */
    public function setChildCategoryTreeNodes($child_category_tree_nodes)
    {
        if (is_null($child_category_tree_nodes)) {
            throw new \InvalidArgumentException('non-nullable child_category_tree_nodes cannot be null');
        }
        $this->container['child_category_tree_nodes'] = $child_category_tree_nodes;

        return $this;
    }

    /**
     * Gets leaf_category_tree_node
     *
     * @return bool|null
     */
    public function getLeafCategoryTreeNode()
    {
        return $this->container['leaf_category_tree_node'];
    }

    /**
     * Sets leaf_category_tree_node
     *
     * @param bool|null $leaf_category_tree_node A value of <code>true</code> indicates that the current category tree node is a leaf node (it has no child nodes). A value of <code>false</code> indicates that the current node has one or more child nodes, which are identified by the <b>childCategoryTreeNodes</b> array.<br><br><i>Returned only if</i> the value of this field is <code>true</code>.
     *
     * @return self
     */
    public function setLeafCategoryTreeNode($leaf_category_tree_node)
    {
        if (is_null($leaf_category_tree_node)) {
            throw new \InvalidArgumentException('non-nullable leaf_category_tree_node cannot be null');
        }
        $this->container['leaf_category_tree_node'] = $leaf_category_tree_node;

        return $this;
    }

    /**
     * Gets parent_category_tree_node_href
     *
     * @return string|null
     */
    public function getParentCategoryTreeNodeHref()
    {
        return $this->container['parent_category_tree_node_href'];
    }

    /**
     * Sets parent_category_tree_node_href
     *
     * @param string|null $parent_category_tree_node_href The href portion of the <b>getCategorySubtree</b> call that retrieves the subtree below the parent of this category tree node.<br><br><i>Not returned if</i> the current category tree node is the root node of its tree.
     *
     * @return self
     */
    public function setParentCategoryTreeNodeHref($parent_category_tree_node_href)
    {
        if (is_null($parent_category_tree_node_href)) {
            throw new \InvalidArgumentException('non-nullable parent_category_tree_node_href cannot be null');
        }
        $this->container['parent_category_tree_node_href'] = $parent_category_tree_node_href;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


