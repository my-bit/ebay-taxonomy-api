<?php
/**
 * ErrorTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Taxonomy
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Taxonomy API
 *
 * Use the Taxonomy API to discover the most appropriate eBay categories under which sellers can offer inventory items for sale, and the most likely categories under which buyers can browse or search for items to purchase. In addition, the Taxonomy API provides metadata about the required and recommended category aspects to include in listings, and also has two operations to retrieve parts compatibility information.
 *
 * The version of the OpenAPI document: v1.0.1
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace MyBit\Ebay\Taxonomy\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * ErrorTest Class Doc Comment
 *
 * @category    Class
 * @description This type defines the fields that can be returned in an error.
 * @package     MyBit\Ebay\Taxonomy
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class ErrorTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "Error"
     */
    public function testError()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "category"
     */
    public function testPropertyCategory()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "domain"
     */
    public function testPropertyDomain()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "error_id"
     */
    public function testPropertyErrorId()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "input_ref_ids"
     */
    public function testPropertyInputRefIds()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "long_message"
     */
    public function testPropertyLongMessage()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "message"
     */
    public function testPropertyMessage()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "output_ref_ids"
     */
    public function testPropertyOutputRefIds()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "parameters"
     */
    public function testPropertyParameters()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "subdomain"
     */
    public function testPropertySubdomain()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }
}
